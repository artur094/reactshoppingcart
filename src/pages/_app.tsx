import type { AppProps } from "next/app";

import "../global.css";
import { Provider } from "react-redux";
import { createStore } from "redux";
import cartReducer from "../components/reducers/cartReducer";

const store = createStore(cartReducer);

const QogitaApp = ({ Component, pageProps }: AppProps): JSX.Element => (
  <Provider store={store}>
    <Component {...pageProps} />
  </Provider>
);

export default QogitaApp;
