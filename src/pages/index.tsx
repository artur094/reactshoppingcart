import Layout from "../components/layout";
import React from "react";
import ProductTable from "../components/layout/product-table";
import { Product } from "../types";
import { fetchAllProducts } from "../utils/api-products";

type HomePageState = {
  products: Product[];
  page: number;
  numberOfPages: number;
};

class HomePage extends React.Component<{}, HomePageState> {
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.state = {
      products: [],
      page: 0,
      numberOfPages: 0,
    };

    this.goToPage(1);
  }

  goToPage(page: number): void {
    if (page !== this.state.page) {
      fetchAllProducts(page)
        .then((result) => {
          this.setState({
            products: result.results,
            numberOfPages: result.count / 20,
            page: page,
          });
        })
        .catch((err) => console.error("Error ", err));
    }
  }

  render() {
    return (
      <Layout>
        <div className="mb-12">{ProductTable(this.state.products)}</div>
        {this.renderPageNumbers()}
      </Layout>
    );
  }

  renderPageNumbers() {
    const productList = Array.from(
      Array(this.state.numberOfPages),
      (_, index) => index + 1
    ).map((n) => (
      <li key={n} className="mr-6">
        <a
          className={
            (this.state.page === n
              ? "font-bold text-white"
              : "text-blue-100 font-thin") + " cursor-pointer"
          }
          onClick={() => this.goToPage(n)}
        >
          {n}
        </a>
      </li>
    ));

    return (
      <div className="fixed bottom-0 left-0 w-screen bg-blue-600">
        <ul className="flex flex-row container mx-auto h-12 items-center text-xl">
          {productList}
        </ul>
      </div>
    );
  }
}

export default HomePage;
