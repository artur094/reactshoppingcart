import Layout from "../../components/layout";
import React from "react";
import ProductTable from "../../components/layout/product-table";
import { connect } from "react-redux";
import { Cart, CartProduct } from "../../types";

type CartProps = {
  products: CartProduct[];
  total: string;
  currency: string;
};

class CartPage extends React.Component<CartProps, {}> {
  render() {
    const cartTable = ProductTable(
      this.props.products.map((item) => item.product)
    );
    const cart =
      this.props.products.length > 0 ? (
        <div>
          <div className="border-b-2">{cartTable}</div>
          <div className="flex flex-row bg-white w-full h-8 items-end text-xl mt-3">
            Total of &nbsp;<span className="text-3xl">{this.props?.total}</span>
            &nbsp;<span className="text-sm">{this.props?.currency}</span>
          </div>
        </div>
      ) : (
        <p className="italic">Empty cart</p>
      );

    return (
      <Layout>
        <div className="mb-10">{cart}</div>
      </Layout>
    );
  }
}

const mapStateToProps = (state: Cart) => {
  return {
    products: state.products,
    total: state.products
      .reduce(
        (sum, product) =>
          sum + product.quantity * product.product.recommendedRetailPrice,
        0
      )
      .toFixed(2),
    currency:
      state.products.length > 0
        ? state.products[0].product.recommendedRetailPriceCurrency
        : "",
  };
};

export default connect(mapStateToProps)(CartPage);
