import ProductRecord from "./product-record";
import { Product } from "../../../types";

const ProductTable = (products: Product[]) => {
  return (
    <div key="product-table" className="divide-y divide-blue-200">
      {products.map((product) => (
        <ProductRecord key={product.gtin} product={product} />
      ))}
    </div>
  );
};

export default ProductTable;
