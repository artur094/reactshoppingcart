import { Cart, Product } from "../../../../types";
import { addToCart, removeFromCart } from "../../../actions/cartActions";
import { connect } from "react-redux";
import React from "react";
import Image from "next/image";

type ProductRecordProps = {
  addProduct: (product: Product, quantity: number) => {};
  removeProduct: (product: Product, quantity: number) => {};
  product: Product;
  quantity: number;
};

class ProductRecord extends React.Component<ProductRecordProps> {
  addProductToCart() {
    this.props.addProduct(this.props.product, 1);
  }

  removeProductFromCart() {
    this.props.removeProduct(this.props.product, 1);
  }

  render() {
    const price = (
      <span
        key={this.props.product.recommendedRetailPrice}
        className="text-3xl"
      >
        {this.props.product.recommendedRetailPrice}{" "}
        <span className="text-sm">
          {this.props.product.recommendedRetailPriceCurrency}
        </span>
      </span>
    );
    return (
      <div
        key={this.props.product.gtin}
        className="flex flex-col md:flex-row justify-between py-3"
      >
        <div className="flex flex-row">
          <div className="w-24 mr-3" key={this.props.product.imageUrl}>
            <Image
              src={this.props.product.imageUrl}
              alt="Product Image"
              width={500}
              height={500}
            />
          </div>
          <div
            key={this.props.product.name + "-record"}
            className="flex justify-between w-full"
          >
            <div
              key={this.props.product.name + "-detail"}
              className="flex flex-col"
            >
              <div key={this.props.product.name} className="text-2xl">
                {this.props.product.name}
              </div>
              <div
                key={this.props.product.brandName}
                className="italic text-lg font-light"
              >
                {this.props.product.categoryName} -{" "}
                {this.props.product.brandName}
              </div>
              <div className="mt-3 hidden md:block">{price}</div>
            </div>
          </div>
        </div>
        <div className="flex align-center justify-between h-8 mt-3 md:mt-0">
          <div className="md:hidden">{price}</div>
          <div
            key={this.props.product.gtin + "-cart"}
            className="flex flex-row justify-end md:justify-center"
          >
            <button
              className="shadow bg-blue-500 hover:bg-blue-700 text-white font-bold px-4 rounded-l-lg"
              onClick={() => this.removeProductFromCart()}
            >
              -
            </button>
            <input
              key={this.props.product.gtin + "-" + this.props.quantity}
              className="shadow w-16 text-center appearance-none w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              readOnly
              value={this.props.quantity}
            />
            <button
              className="shadow bg-blue-500 hover:bg-blue-700 text-white font-bold px-4 rounded-r-lg"
              onClick={() => this.addProductToCart()}
            >
              +
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: Cart, props: { product: { gtin: string } }) => {
  const selectedProduct = state.products.find(
    (item) => item.product.gtin == props.product.gtin
  );
  return {
    selectedProduct: props.product,
    quantity: selectedProduct?.quantity ?? 0,
  };
};
const mapDispatchToProps = (
  dispatch: (arg0: { type: string; product: Product; quantity: number }) => {}
) => {
  return {
    addProduct: (product: Product, quantity: number) =>
      dispatch(addToCart(product, quantity)),
    removeProduct: (product: Product, quantity: number) =>
      dispatch(removeFromCart(product, quantity)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductRecord);
