import Link from "next/link";

type Props = {
  children: React.ReactNode;
};

const Layout = ({ children }: Props) => (
  <div>
    <div className="py-3 sticky top-0 bg-blue-600 z-10 text-white font-light">
      <div className="flex justify-between container mx-auto px-4">
        <div className="font-bold">Qogita</div>
        <nav>
          <ul className="flex gap-4">
            <li>
              <Link href="/">
                <a className="underline">Products</a>
              </Link>
            </li>
            <li>
              <Link href="/cart">
                <a className="underline">Your Cart</a>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <div className="container mx-auto px-4">{children}</div>
  </div>
);

export default Layout;
