import { Cart, Product } from "../../types";
import { ADD_TO_CART, REMOVE_FROM_CART } from "../actions/cartActions";

const initState: Cart = {
  products: [],
  total: 0,
};

const cartReducer = (
  state = initState,
  action: { type: string; product: Product; quantity: number }
) => {
  if (action.type === ADD_TO_CART) {
    let productCart = state.products.find(
      (item) => item.product.gtin === action.product.gtin
    );

    if (productCart == null) {
      productCart = { product: action.product, quantity: 0 };
      state.products.push(productCart);
    }

    productCart.quantity += action.quantity;

    return {
      ...state,
      total: state.total + action.quantity,
    };
  } else if (action.type === REMOVE_FROM_CART) {
    const productCart = state.products.find(
      (item) => item.product.gtin === action.product.gtin
    );
    let removedQuantity = 0;

    if (productCart != null) {
      removedQuantity = Math.min(productCart.quantity, action.quantity);

      if (removedQuantity == productCart.quantity) {
        state.products = state.products.filter(
          (item) => item.product.gtin != action.product.gtin
        );
      } else {
        productCart.quantity -= removedQuantity;
      }
    }

    return {
      ...state,
      total: state.total - removedQuantity,
    };
  }
  return state;
};
export default cartReducer;
