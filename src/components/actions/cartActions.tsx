import { Product } from "../../types";

export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";

export const addToCart = (product: Product, quantity: number) => {
  return {
    type: ADD_TO_CART,
    product,
    quantity,
  };
};

export const removeFromCart = (product: Product, quantity: number) => {
  return {
    type: REMOVE_FROM_CART,
    product,
    quantity,
  };
};
