export async function fetchAllProducts(page: number) {
  const queryParams = page > 0 ? "?page=" + page : "";
  return await fetch(
    process.env.NEXT_PUBLIC_API_URL + "/api/products" + queryParams
  ).then((res) => res.json());
}
